var canvasObjects = {};
var elementToPush = null;
    
//Function to generate canvases in range of 3 to 5.
function generateCanvas() {
    /**Generate random number between 3 to 5 */
    var mininum = 3, maximum = 5;
    let randomNumber = Math.floor(Math.random() * (maximum - mininum + 1)) + mininum;
    /**Generate those many number of canvases */
    let htmlCanvas = null;
    let parent = document.querySelector('#canvas-container');
    let option = null;
    let canvasSelector = document.querySelector('#canvas-selector');
    /**Clear already added elements */
    while (parent.firstChild) parent.removeChild(parent.firstChild);
    while (canvasSelector.firstChild) canvasSelector.removeChild(canvasSelector.firstChild);
    canvasObjects = {};
    for(let i = 1; i <= randomNumber; i++) {
        canvasWrapper = document.createElement('div');
        canvasWrapper.setAttribute('id','canvas_wrapper_'+i);
        canvasWrapper.addEventListener('mousemove', function(event) {
            console.log(this.id,elementToPush);
            if(elementToPush) {
                if(Array.isArray(elementToPush)) {
                    elementToPush.forEach((value) => {
                        canvasObjects[this.id].add(generateShape(value));
                    });
                } else {
                    canvasObjects[this.id].add(generateShape(elementToPush));
                }
               
                elementToPush = null;
            }
         });
        htmlCanvas = document.createElement('canvas');
        htmlCanvas.setAttribute('id','canvas_'+i);
        htmlCanvas.setAttribute('width','300');
        htmlCanvas.setAttribute('height','300');
        htmlCanvas.setAttribute('class','canvas');
        
        canvasWrapper.appendChild(htmlCanvas)
        parent.appendChild(canvasWrapper);
        /**Generate fabic handles */
        canvasObjects['canvas_wrapper_'+i] =(new fabric.Canvas('canvas_'+i));
        /**Generate options for selection */
        option = document.createElement( 'option' );
        option.text = 'Canvas '+i;
        option.value = 'canvas_wrapper_'+i;
        canvasSelector.add( option );
    }
}
    function generateIntialShape() {
        let shape = generateShape({type:document.querySelector('#item-selector').value});
        canvasObjects[ document.querySelector('#canvas-selector').value ].add(shape);
        setEventListner();
    }
    function generateShape(option) {
        switch(option.type) {
            case 'circle':
            return generateCircle(option);
            case 'rect':
                return generateRect(option);
            case 'triangle':
                return generateTriangle(option);
        }   
       
    }

function setEventListner() {
    Object.values(canvasObjects).map((canvas) => {
        canvas.on({
            'object:moving': function(e) {
                e.target.opacity = 0.5;
            },
            'object:modified': function(e) {
                e.target.opacity = 1;
                if(!e.target.isOnScreen({fully:true})) {
                    pushToCache(e.target);
                    canvas.discardActiveObject().renderAll();
                }
            }
          });
    });
}

function pushToCache(target) {
    if(target.type == 'activeSelection') {
        elementToPush =[];
        for(element of target._objects) {
            elementToPush.push({
                type :element.type,
                width: element.width ,
                height: element.height,      
                fill: element.fill
            });
        }
        return;
    }
    elementToPush = {
        type :target.type,
        width: target.width ,
        height: target.height,      
        fill: target.fill
    };
}

function generateRect(options) {
   return new fabric.Rect({
        left: options.left || 40,
        top: options.top || 40,
        width: options.width || 50,
        height: options.height || 50,      
        fill: options.fill || 'green'
    });  
}
function generateTriangle(options) {
    return  new fabric.Triangle({
        width: options.width || 20, 
        height: options.height || 30, 
        fill: options.fill || 'blue', 
        left:  options.left || 50, 
        top: options.top || 50
      }); 
}
function generateCircle(options) {
    return new fabric.Circle({
        left: options.left || 40,
        top:  options.top ||40,
        radius: options.radius || 50,     
        fill:  options.fill ||'red',
    });  
}